//
//  main.m
//  Random Items
//
//  Created by Jessica Brookhouse on 8/30/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BNRItem.h"

int main(int argc, const char * argv[])
{
    @autoreleasepool
    {
        NSMutableArray *items = [[NSMutableArray alloc] init];
        
        // Fill the array with randomized BNRItem objects.
        for (int i = 0; i < 10; i++)
        {
            BNRItem *itemRandom = [BNRItem randomItem];
            [items addObject:itemRandom];
        }
        // Log the description of each item in the items array.
        for (NSString *item in items)
        {
            NSLog(@"%@", item);
        }
        
        // Destroy the array.
        items = nil;
    }
    return 0;
}

