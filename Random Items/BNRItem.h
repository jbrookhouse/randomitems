//
//  BNRItem.h
//  Random Items
//
//  Created by Jessica Brookhouse on 8/30/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNRItem : NSObject
{
    NSString *_itemName;
    NSString *_serialNumber;
    int _valueInDollars;
    NSDate *_dateCreated;
}

+ (instancetype) randomItem;

// Designated initializer.
- (instancetype) initWithItemName:(NSString *)name
                   valueInDollars:(int)value
                     serialNumber:(NSString *)serialNumber;

- (instancetype) initWithItemName:(NSString *)name;

- (void) setItemName:(NSString *)str;
- (NSString *) itemName;

- (void) setSerialNumber:(NSString *)str;
- (NSString *) serialNumber;

- (void) setValueInDollars:(int)v;
- (int) valueInDollars;

- (NSDate *) dateCreated;

@end
