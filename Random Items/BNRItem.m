//
//  BNRItem.m
//  Random Items
//
//  Created by Jessica Brookhouse on 8/30/14.
//  Copyright (c) 2014 Big Nerd Ranch. All rights reserved.
//

#import "BNRItem.h"

@implementation BNRItem

+ (instancetype) randomItem
{
    NSArray *adjectiveList    = @[@"Fluffy", @"Rusty", @"Shiny"];
    NSArray *nounList         = @[@"Bear", @"Spork", @"Mac"];
    
    // Get the index of a random adjective/noun from the lists
    NSInteger randomAdjectiveIndex    = arc4random() % [adjectiveList count];
    NSInteger randomNounIndex         = arc4random() % [nounList count];
    
    NSString *randomName = [NSString stringWithFormat:@"%@ %@",
                            adjectiveList[randomAdjectiveIndex],
                            nounList[randomNounIndex]];
    
    int randomValue = arc4random_uniform(100);
    
    NSString *randomSerialNumber = [NSString stringWithFormat:@"%c%c%c%c%c",
                                    '0' + arc4random_uniform(10),
                                    'A' + arc4random_uniform(26),
                                    '0' + arc4random_uniform(10),
                                    'A' + arc4random_uniform(26),
                                    '0' + arc4random_uniform(10)];
    
    // Initialize the new item with these values before returning. 
    BNRItem *newItem = [[self alloc] initWithItemName:randomName
                                       valueInDollars:randomValue
                                         serialNumber:randomSerialNumber];
    
    return newItem;
}

- (instancetype) initWithItemName:(NSString *)name
                   valueInDollars:(int)value
                     serialNumber:(NSString *)sNumber
{
    // Call the superclass's designated initializer.
    self = [super init];
    
    // Did the superclass's designated initializer succeed?
    if (self)
    {
        // Give the instance variables initial values.
        _itemName = name;
        _serialNumber = sNumber;
        _valueInDollars = value;
        
        // Set _dateCreated to the current date and time.
        _dateCreated = [[NSDate alloc] init];
    }
    
    // Return the address of the newly initialized object.
    return self;
}

- (instancetype) initWithItemName:(NSString *)name
{
    return [self initWithItemName:name
                   valueInDollars:0
                     serialNumber:@""];
}

- (instancetype) init
{
    return [self initWithItemName:@"Item"];
}

- (void) setItemName:(NSString *) str
{
    _itemName = str;
}
- (NSString *) itemName
{
    return _itemName;
}

- (void) setSerialNumber:( NSString *) str
{
    _serialNumber = str;
}
- (NSString *) serialNumber
{
    return _serialNumber;
}

- (void) setValueInDollars:( int) v
{
    _valueInDollars = v;
}
- (int) valueInDollars
{
    return _valueInDollars;
}

- (NSDate *) dateCreated
{
    return _dateCreated;
}

- (NSString *) description
{
    NSString *descriptionString =
        [[NSString alloc] initWithFormat:@"%@ (%@): Worth $%d, recorded on %@.",
                            self.itemName,
                            self.serialNumber,
                            self.valueInDollars,
                            self.dateCreated];
    return descriptionString;
}

@end
